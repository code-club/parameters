import numpy as np
import matplotlib.pyplot as plt

# Draw random (x, y) points in the ([-1, 1], [-1,1]) intervals
x = 1 - 2 * np.random.random_sample(1000)
y = 1 - 2 * np.random.random_sample(1000)
print("1000 random samples have been drawn.")

# Count how many samples are inside a circle of radius 1.
idx_inside_circle = (x**2 + y**2) < 1
idx_outside_circle = (x**2 + y**2) > 1
print(f"{idx_inside_circle.sum()} samples are inside the circle of radius 1, {idx_outside_circle.sum()} are outside.")

# Plot the samples
fig, ax = plt.subplots(1)
ax.scatter(x[idx_inside_circle], y[idx_inside_circle], c='b', alpha=0.8, edgecolor=None)
ax.scatter(x[idx_outside_circle], y[idx_outside_circle], c='r', alpha=0.8, edgecolor=None)
ax.set_aspect("equal")
plt.savefig("pi.png")
plt.show()

# Compute pi as the ratio of points inside the circle vs total points (surface ratio = pi/4)
pi = 4 * idx_inside_circle.sum() / 1000
print(f"pi = {pi}")
